using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MessageHandler;
using MessageHandler.SDK.EventSource;
using Yaus.Framework.AzureSearch;
using Environment = MessageHandler.Environment;

namespace ProjectProfilesToSearch
{
    public class RestoreViewIfNeeded : IStartupTask
    {
        private readonly IBuildViews _viewBuilder;
        private readonly ITrackProjectionCodeChanges _projectionCode;
        private AzureSearchClient _searchclient;
        private string _sourceType;
        private string _index;

        public RestoreViewIfNeeded(IConfigurationSource source, IVariablesSource variables, ITemplatingEngine templating, IBuildViews viewBuilder, ITrackProjectionCodeChanges projectionCode)
        {
            _viewBuilder = viewBuilder;
            _projectionCode = projectionCode;

            dynamic channelVariables = variables.GetChannelVariables(Channel.Current());
            dynamic accountVariables = variables.GetAccountVariables(Account.Current());
            dynamic environmentVariables = variables.GetEnvironmentVariables(Environment.Current());

            var config = source.GetConfiguration<ProjectProfilesToSearchConfig>();

            var serviceName = templating.Apply(config.SearchServiceName, null, channelVariables, environmentVariables, accountVariables);
            var apiKey = templating.Apply(config.SearchServiceApiKey, null, channelVariables, environmentVariables, accountVariables);
            _sourceType = templating.Apply(config.SourceType, null, channelVariables, environmentVariables, accountVariables);
            _index = templating.Apply(config.SearchServiceIndex, null, channelVariables, environmentVariables, accountVariables);

            _searchclient = new AzureSearchClient(serviceName, apiKey);
        }

        public void Run()
        {
            var task = RunInternal();
            task.Wait();
        }

        private async Task RunInternal()
        {
            if (await _projectionCode.HasChanges(typeof(Projection)))
            {
                Trace.TraceInformation("Projection code has changes, restoring.");
                await ClearIndex();
                await PopulateIndex();

                await _projectionCode.RegisterChanges(typeof(Projection));

            }
        }

        private async Task PopulateIndex()
        {
            try
            {
                var Profiles = await _viewBuilder.BuildAsync<SearchEntry>(_sourceType);

                var list = Profiles.Values.ToList();
                if (list.Count > 0)
                    await _searchclient.Upsert(_index, list.ToArray<object>());
            }
            catch (Exception ex)
            {
                Trace.TraceError("Error occured while populating index " + ex);
            }
            
        }

        private async Task ClearIndex()
        {
            var query = new QueryBuilder()
                 .SearchMode(SearchMode.All)
                 .Filter("type", "eq", "Profile");

            var results = await _searchclient.Search(_index, query.ToQueryString());
            foreach (var result in results.value)
            {
                await _searchclient.Delete(_index, "id", (string)result.id);
            }

        }
    }
}