using System;
using System.Spatial;

namespace ProjectProfilesToSearch
{
    //public class ProfileEntry
    //{
    //    public string ProfileId { get; set; }
    //    public string Name { get; set; }
    //    public string About { get; set; }
    //    public string Email { get; set; }
    //    public string Blog { get; set; }
    //    public string Website { get; set; }
    //    public string Twitter { get; set; }

    //    public DateTime DatePosted { get; set; }
    //    public DateTime DatePublished { get; set; }

    //    public string PublicationState { get; set; }

    //}

    public class SearchEntry
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? Date { get; set; }
        public string Type { get; set; }
        public string Object { get; set; }
        public string Account { get; set; }
        public string Tenant { get; set; }
        public string User { get; set; }

        public string State1 { get; set; }
        public DateTime? State1ValidFrom { get; set; }
        public DateTime? State1ValidTo { get; set; }

        public string State2 { get; set; }
        public DateTime? State2ValidFrom { get; set; }
        public DateTime? State2ValidTo { get; set; }

        public string State3 { get; set; }
        public DateTime? State3ValidFrom { get; set; }
        public DateTime? State3ValidTo { get; set; }

        public string State4 { get; set; }
        public DateTime? State4ValidFrom { get; set; }
        public DateTime? State4ValidTo { get; set; }

        public string Reference1 { get; set; }
        public string Reference2 { get; set; }
        public string Reference3 { get; set; }
        public string Reference4 { get; set; }

        public string Searchable1 { get; set; }
        public string Searchable2 { get; set; }
        public string Searchable3 { get; set; }
        public string Searchable4 { get; set; }

        public string Filter1 { get; set; }
        public string Filter2 { get; set; }
        public string Filter3 { get; set; }
        public string Filter4 { get; set; }

        public int? Int1 { get; set; }
        public int? Int2 { get; set; }
        public int? Int3 { get; set; }
        public int? Int4 { get; set; }

        public long? Long1 { get; set; }
        public long? Long2 { get; set; }
        public long? Long3 { get; set; }
        public long? Long4 { get; set; }

        public double? Double1 { get; set; }
        public double? Double2 { get; set; }
        public double? Double3 { get; set; }
        public double? Double4 { get; set; }

        public GeographyPoint Geo1 { get; set; }
        public GeographyPoint Geo2 { get; set; }
        public GeographyPoint Geo3 { get; set; }
        public GeographyPoint Geo4 { get; set; }

        public DateTime? Date1 { get; set; }
        public DateTime? Date2 { get; set; }
        public DateTime? Date3 { get; set; }
        public DateTime? Date4 { get; set; }

        public string[] Collection1 { get; set; } = new string[0];
        public string[] Collection2 { get; set; } = new string[0];
        public string[] Collection3 { get; set; } = new string[0];
        public string[] Collection4 { get; set; } = new string[0];
    }
}