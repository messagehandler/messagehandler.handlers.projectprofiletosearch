using MessageHandler;
using MessageHandler.SDK.EventSource;

namespace ProjectProfilesToSearch
{
    public class AddToContainer : IInitialization
    {
        public void Init(IContainer container)
        {
            var source = container.Resolve<IConfigurationSource>();
            var config = source.GetConfiguration<ProjectProfilesToSearchConfig>();
            container.UseEventSourcing(config.ConnectionString)
                    .EnableProjections(config.ConnectionString);
        }
    }
}