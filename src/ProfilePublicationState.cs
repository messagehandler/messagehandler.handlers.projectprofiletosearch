namespace ProjectProfilesToSearch
{
    public enum ProfilePublicationState
    {
        Draft,
        Published
    }
}