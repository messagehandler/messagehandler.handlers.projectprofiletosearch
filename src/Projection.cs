using System.ServiceModel.Security.Tokens;
using MessageHandler;
using MessageHandler.SDK.EventSource;
using MessageHandler.Contracts.Profiles;

namespace ProjectProfilesToSearch
{
    public class Projection :
        IProjection<SearchEntry, ProfileDrafted>,
        IProjection<SearchEntry, ProfilePublished>,
        IProjection<SearchEntry, ProfileUnpublished>,
        IProjection<SearchEntry, ProfileUpdated>
    {
        public void Project(SearchEntry record, ProfileDrafted msg)
        {
            record.Id = msg.Details.ProfileId;
            record.Title = msg.Details.Name;
            record.Description = msg.Details.About;
            record.Date1 = msg.Details.DatePosted;
            record.Date2 = msg.Details.DatePublished;
            record.State1 = msg.Details.PublicationState.ToString();
            record.Object = Json.Encode(msg.Details);
            record.Type = "Profile";
        }

        public void Project(SearchEntry record, ProfilePublished msg)
        {
            record.Id = msg.Details.ProfileId;
            record.Title = msg.Details.Name;
            record.Description = msg.Details.About;
            record.Date1 = msg.Details.DatePosted;
            record.Date2 = msg.Details.DatePublished;
            record.State1 = msg.Details.PublicationState.ToString();
            record.Object = Json.Encode(msg.Details);
            record.Type = "Profile";
        }

        public void Project(SearchEntry record, ProfileUnpublished msg)
        {
            record.Id = msg.Details.ProfileId;
            record.Title = msg.Details.Name;
            record.Description = msg.Details.About;
            record.Date1 = msg.Details.DatePosted;
            record.Date2 = msg.Details.DatePublished;
            record.State1 = msg.Details.PublicationState.ToString();
            record.Object = Json.Encode(msg.Details);
            record.Type = "Profile";
        }

        public void Project(SearchEntry record, ProfileUpdated msg)
        {
            record.Id = msg.Details.ProfileId;
            record.Title = msg.Details.Name;
            record.Description = msg.Details.About;
            record.Date1 = msg.Details.DatePosted;
            record.Date2 = msg.Details.DatePublished;
            record.State1 = msg.Details.PublicationState.ToString();
            record.Object = Json.Encode(msg.Details);
            record.Type = "Profile";
        }
    }
}